const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const render = require('koa-ejs');
const path = require('path');
const mysql = require('mysql2/promise');

const app = new Koa();
const router = new Router();

render(app,{
    root:path.join(__dirname,'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false,
    debug:true
});

router.get('/',async (ctx,next)=>{
    const pool = mysql.createPool({
        host:'localhost',
        user:'root',
        password:'',
        database:'codecamp2'
    });
    const [rows,fields] = await pool.query('select*from user');
    const title = 'Landing page';
    await ctx.render('index',{rows,title});
});

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);