const Koa = require('koa');
const serve = require('koa-static');
const path = require('path');

const app = new Koa();

app.use(serve(path.join(__dirname,'public')));
app.use(require('./src/models').routes);
app.use(require('./src/models').allowedMethods);

app.listen(3000);