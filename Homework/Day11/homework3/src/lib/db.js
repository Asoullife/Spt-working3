const mysql = require('mysql2/promise');

const connection = async () => {
    const pool = await mysql.createPool({
        connectionLimit: 10,
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'codecamp2'
    });
    return pool;
}

module.exports = { connection };