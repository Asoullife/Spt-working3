const db = require('../lib/db');

const data = async (ctx, next) => {
    const pool = await db.connection();
    const [rows, fields] = await pool.query('select*from user');
    return ctx.body = rows;
}

module.exports = { data };