const fs = require('fs');
const util = require('util');
const path = require('path');

const data = async (ctx,next)=>{
    const readFile = util.promisify(fs.readFile);
    const read = await readFile(path.join(__dirname,'../lib/homework2_1.json'),'utf8');
    return ctx.body = read;
}

module.exports = {data};