const Router = require('koa-router');
const fromDb = require('./from_database');
const fromJson = require('./from_file');
const router = new Router();

router.get('/from_database',fromDb.data);
router.get('/from_file',fromJson.data);

module.exports.routes = router.routes();
module.exports.allowedMethods = router.allowedMethods();