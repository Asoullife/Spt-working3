const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path');
const mysql = require('mysql2/promise');
const fs = require('fs');
const util = require('util');

const app = new Koa();
const router = new Router();

router.get('/from_database', async (ctx, next) => {
    const pool = await mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'codecamp2'
    });
    const [rows, fields] = await pool.query('select*from user');
    ctx.body = { rows };
    ctx.variable = rows;
    await next();
});

router.get('/from_file', async (ctx, next) => {
    const readFile = util.promisify(fs.readFile);
    const rows = await readFile(path.join(__dirname, 'public/json/homework2_1.json'), 'utf8');
    ctx.body = rows;
    ctx.variable = rows;
    await next();
});

const middleware = async (ctx, next) => {
    const date = new Date();
    const additionalData = {
        userId: 1,
        dateTime: `${date.getDate()}-${date.getMonth()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
    };

    let data;
    if (typeof (ctx.variable) === 'string') {
        data = JSON.parse(ctx.variable);
    }
    else {
        data = ctx.variable;
    }
    ctx.body = { data, additionalData };
    await next();
}

app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());
app.use(middleware);

app.listen(3000);