import React from "react";
import PropTypes from "prop-types";

const Item = props => (
  <div>
    <input value={props.item.text} readOnly />
    <button onClick={() => props.handleEdit(props.item.id)}>Edit</button>
    <button onClick={() => props.handleDel(props.item.id)}>Del</button>
  </div>
);

// class Item extends React.Component {
//     state = {
//        text: this.props.item.text 
//     }
    
//     componentWillReceiveProps(newProps) {
//         if (newProps.item.text !== this.props.text) {
//             this.setState({text: newProps.item.text})
//         } 
//     }

//     render() {
//         return (
//             <div>
//                 <input value={this.state.text} readOnly/>
//                 <button onClick={()=>this.props.handleEdit(this.props.item.id)}>Edit</button>
//                 <button onClick={()=>this.props.handleDel(this.props.item.id)}>Del</button>
//             </div>
//         )
//     }
// }

export default Item;

Item.propTypes = {
  onClick: PropTypes.func
};
