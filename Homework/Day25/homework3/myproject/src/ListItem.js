import React from 'react';
import Item from './Item';

const ListItem = (props) =>(
    <div>
        {props.todoList.map(todo=>
        <Item key={todo.id} item={todo}
        handleEdit = {props.handleEdit}
        handleDel = {props.handleDel}
        />)}
    </div>
)

export default ListItem;