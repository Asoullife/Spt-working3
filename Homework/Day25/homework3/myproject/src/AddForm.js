import React,{Component} from 'react';
import PropTypes from 'prop-types';

class AddForm extends Component{
    state={
        text:''
    }
    handleChange = (e)=>{
        this.setState({[e.target.name]:e.target.value});
    }
    handleSubmit =(e)=>{
        e.preventDefault();
        this.props.onSubmit(this.state);
        this.setState({text:''});
    }
    render(){
        const {text} = this.state;
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input name="text" value={text} onChange={this.handleChange} />
                    <button>Add</button>
                </form>
            </div>
        )
    }
}

export default AddForm;

AddForm.propTypes ={
    onSubmit:PropTypes.func
}