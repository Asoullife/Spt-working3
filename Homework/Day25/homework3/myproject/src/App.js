import React, { Component } from "react";
import AppForm from './AddForm';
import ListItem from './ListItem';

class App extends Component {
  state={
    text:'',
    nextId:1,
    todos:[]
  }
  onSubmit = (todo) =>{
    const {nextId} = this.state;
    this.setState({
      todos:[...this.state.todos,{id:nextId,text:todo.text}],
      nextId:nextId+1,
      text:''
    });
  }
  handleEdit = (id)=>{
    const textEdit = prompt("Edit","");
    this.setState({ 
      todos:this.state.todos.map(todo=>(todo.id===id)?{id,text:textEdit}:todo)
    })
  }
  handleDel = (id) =>{
    this.setState({
      todos:this.state.todos.filter(todo=>(todo.id!==id))
    })
  }
  render(){
    return(
      <div>
        <AppForm onSubmit={this.onSubmit}/>
        <ListItem todoList={this.state.todos}
        handleEdit={this.handleEdit}
        handleDel={this.handleDel}
        />
      </div>
    )
  }
}

export default App;
