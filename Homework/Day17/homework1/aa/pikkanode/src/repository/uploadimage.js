
const pool = require('../db')

const uploads = async (filename, caption, create_by) => {
	const result = await pool.query(`
		insert into pictures
			(id, caption,created_by)
		values
			(?, ?,?)
	`, [filename, caption, create_by])

	return result[0].insertId
}

module.exports = {
	uploads
}
