const user = require('./user')
const uploadimage = require('./uploadimage')

module.exports = {
	user,
	uploadimage
}
