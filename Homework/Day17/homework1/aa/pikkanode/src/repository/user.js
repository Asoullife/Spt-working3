
const pool = require('../db')

const register = async (email, password) => {
	const result = await pool.query(`
		insert into users
			(email, password)
		values
			(?, ?)
	`, [email, password])
	//console.log(result)

	// ????
	return result[0].insertId
}

module.exports = {
	register
}
