const { uploadimage } = require('../../repository')

const path = require('path');
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')

const getHandler = async (ctx) => {
	await ctx.render('upload');
}

const pictureDir = path.join(process.cwd(), 'public', 'images')

const allowFileType = {
	'image/png': true,
	'image/jpeg': true
}

const postHandler = async ctx => {
	try {
		if (!allowFileType[ctx.request.files.photo.type]) {
			throw new Error('file type not allow')
		}
		console.log(ctx.request.body.caption)
		console.log(ctx.request.body.detail)
		console.log(ctx.request.files.photo.name)
		console.log(ctx.request.files.photo.path)
		const fileName = uuidv4()
		await fs.rename(ctx.request.files.photo.path, path.join(pictureDir, fileName))
		console.log(fileName);
		ctx.status = 303
		const create_by = 1;
		const pic = await uploadimage.uploads(fileName,ctx.request.body.caption,create_by)
		
		ctx.redirect('/create')
	} catch (e) {
		// handle error here
		ctx.status = 400
		ctx.body = e.message
		fs.remove(ctx.request.files.photo.path)
	}
}



module.exports = {
	getHandler,
	postHandler
}
