const Router = require('koa-router');
const router = new Router();
const signIn = require('./signin');
const signup = require('./signup');

router.get('/signin',signIn.getHandler);
router.post('/signin',signIn.postHandler);

module.exports = router.routes()