const getHandler = async (ctx)=>{
    await ctx.render('signin');
};

const postHandler = async (ctx)=>{
    console.log('email', ctx.request.body.email);
    console.log('password', ctx.request.body.password);
    ctx.redirect('/signin');
}

module.exports = {
    getHandler,
    postHandler
}