const Koa = require('koa');
const serve = require('koa-static');
const path = require('path');
const render = require('koa-ejs');
const koaBody = require('koa-body');

const app = new Koa();

render(app,{
    root:path.join(process.cwd(),'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
})

app.use(koaBody());
app.use(require('./route'));
app.use(serve(path.join(process.cwd(),'public')));

app.listen(8000);