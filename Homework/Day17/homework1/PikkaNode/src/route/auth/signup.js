const pool = require("../../db");

const getHendler = async ctx => {
  await ctx.render("signup");
};

const postHendler = async ctx => {
  const { email, password, confirm_password } = ctx.request.body;
  if (password !== confirm_password) {
    return (ctx.body = "incorrct password");
  }
  await pool.query(
    `
    insert into users
        (email,password)
    values
        (?,?)
    `,
    [email, password]
  );
  ctx.redirect("/signin");
};

module.exports = {
  getHendler,
  postHendler
};
