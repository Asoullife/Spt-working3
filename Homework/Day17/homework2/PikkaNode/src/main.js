const Koa = require('koa');
const serve = require('koa-static');
const render = require('koa-ejs');
const koaBody = require('koa-body');
const path = require('path');

const app = new Koa();

render(app,{
    root:path.join(process.cwd(),'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
});

app.use(koaBody({multipart:true}));
app.use(require('./route'));
app.use(serve(path.join(process.cwd(),'public')));

app.listen(8000);