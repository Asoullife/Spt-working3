const Router = require('koa-router');
const signin = require('./signin');
const signup = require('./signup');

const router = new Router();

router.get('/signin',signin.getHandler);
router.post('/signin',signin.postHendler);
router.get('/signup',signup.getHandler);
router.post('/signup',signup.postHanler);

module.exports = router.routes();