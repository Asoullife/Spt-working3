const {user} = require('../../repository');

const getHandler = async(ctx)=>{
    await ctx.render('signup');
}
const postHanler = async(ctx)=>{
    const {email,password,confirm_password} = ctx.request.body;
    if(password !== confirm_password){return ctx.body = 'incorrect'};
    const userId = await user.register(email,password);
    ctx.redirect('/signin');
}

module.exports = {
    getHandler,
    postHanler
}