const uuidv4 = require("uuid/v4");
const fs = require("fs-extra");
const path = require("path");
const { upload } = require("../../repository");

const getHendler = async ctx => {
  await ctx.render("upload");
};

const pictureDir = path.join(process.cwd(), "public", "images");
const allowFileType = {
  "image/jpg": true,
  "image/png": true,
  "image/jpeg": true
};

const postHendler = async ctx => {
  try {
    if (!allowFileType[ctx.request.files.pic.type]) {
      throw new Error("file type not allow");
    }
    const caption = ctx.request.body.caption;
    const filename = uuidv4();
    const created_by = 1;
    console.log(ctx.request.files)
    await fs.rename(ctx.request.files.pic.path, path.join(pictureDir, filename));
    const picId = await upload.pictures(filename, caption, created_by);
    ctx.redirect('/');
  } catch (e) {
      ctx.stutes = 400;
      ctx.body = e.message;
      fs.remove(ctx.request.files.pic.path);
  }
};

module.exports = {
  getHendler,
  postHendler
};
