const Router = require('koa-router');
const create = require('./create');
const router = new Router();

router.get('/create',create.getHendler);
router.post('/create',create.postHendler);

module.exports = router.routes();