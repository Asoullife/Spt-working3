const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path');
const fetch = require('isomorphic-unfetch');
const mysql = require('mysql2/promise');

const app = new Koa();
const router = new Router();

router.get('/add',async (ctx,next)=>{
    await fetch('https://randomuser.me/api/')
    .then(res=>res.json())
    .then(async data=>{
        const connection = await mysql.createConnection({
            host:'localhost',
            user:'root',
            password:'',
            database:'bookstore2'
        });
        await connection.query(
            `insert into staff(firstname,lastname,age) values
            ("${data.results[0].name.first}","${data.results[0].name.last}","${data.results[0].dob.age}")`);
        //await connection.query(`update staff`);
        ctx.body = '';
    });
});

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);