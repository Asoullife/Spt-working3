const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const render = require('koa-ejs');
const mysql = require('mysql2/promise');
const path = require('path');

const app = new Koa();
const router = new Router();

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

const connection = async () => {
    const pool = await mysql.createPool({
        connectionLimit: 10,
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'bookstore'
    });
    return pool;
}

router.get('/staff', async (ctx, next) => {
    const pool = await connection();
    const [rows, fields] = await pool.query('select*from staff');
    const title = 'Staff_List';
    await ctx.render('staff', { rows, title });
});

router.get('/book', async (ctx, next) => {
    const pool = await connection();
    const [rows, fields] = await pool.query('select*from book');
    const title = 'Book_List';
    await ctx.render('book', { rows, title });
});

app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);