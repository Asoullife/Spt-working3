peopleSalary = [
    {
        "id": 1001,
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": 40000
    },
    {
        "id": 1002,
        "firstname": "Tony",
        "lastname": "Stark",
        "company": "Marvel",
        "salary": 1000000
    },
    {
        "id": 1003,
        "firstname": "Somchai",
        "lastname": "Jaidee",
        "company": "Love2work",
        "salary": 20000
    },
    {
        "id": 1004,
        "firstname": "Monkey D",
        "lastname": "Luffee",
        "company": "One Piece",
        "salary": 9000000
    }
]
let head = "";
for (let i in peopleSalary[0]) {
    head += `<th>${i}</th>`;
}
let body = "";
let yearSalary = 3;
for (let i = 0; i < peopleSalary.length; i++) {
    body += `<tr>`;
    for (let j in peopleSalary[i]) {
        if (j === 'salary') {
            body += `<td><ol>`;
            let sararyUp = peopleSalary[i][j];
            for (let k = 0; k < yearSalary; k++) {
                body += `<li>${sararyUp}</li>`;
                sararyUp = (sararyUp *0.1)+ sararyUp;
            }
            body += `</td></ol>`;
        }
        else {
            body += `<td>${peopleSalary[i][j]}</td>`;
        }
    }
    body += `</tr>`
}
let table = `<thead><tr>${head}</tr></thead><tbody>${body}</tbody>`;
$(function () {
    $("#table").append(table);
});