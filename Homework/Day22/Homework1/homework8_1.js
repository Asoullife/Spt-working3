class Employee {
    constructor(firstname, lastname, salary) {
        let _salary = salary; // simulate private variable
        this.firstname = firstname;
        this.lastname = lastname;
        this.getSalary = function () {
            return salary;
        }
        this.dressCode = 'tshirt';
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        if (newSalary > this.getSalary()) {
            return newSalary;
        }
        else {
            return false;
        }
    }
    getSalary() {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
    }
    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    _fire(employee) {
        console.log(`${employee.firstname} has been fired! Dress with :${employee.dressCode}`);
    }
    _hire(employee) {
        console.log(`${employee.firstname} has been hired back! Dress with :${employee.dressCode}`);
    }
    _seminar() {
        console.log(`He is going to seminar Dress with :${this.dressCode}`);
    }
    increaseSalary(employee, newSalary) {
        if(employee.setSalary(newSalary)===false){
            console.log(`${employee.firstname}'s salary is less than before!!`);
        }
        else{
            console.log(`${employee.firstname}'s salary has been set to ${newSalary}`);
        }
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };
    gossip(name, say) {
        console.log(name.firstname, say);
    }
}

let somchai = new CEO("Somchai", "Sudlor", 30000);
let somsri = new Employee("Somsri", "Sudsuay", 22000);
somchai.gossip(somsri, "Today is very cold!");
somchai.work(somsri);

somchai.increaseSalary(somsri, 20);
somchai.increaseSalary(somsri, 25000);
