class Employee {
    constructor(firstname, lastname, salary) {
        let _salary = salary; // simulate private variable
        this.firstname = firstname;
        this.lastname = lastname;
        this.getSalary = function () {
            return salary;
        }
        this.dressCode = 'tshirt';
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        if (newSalary > this.getSalary()) {
            return newSalary;
        }
        else {
            return false;
        }
    }
    getSalary() {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}
module.exports.Employee = Employee;