const { SamSungGalaxyNote8 } = require('./Note8.js');
const { SamSungGalaxyS8 } = require('./S8.js');
const { Iphone8 } = require('./Iphone8.js');
const { IphoneX } = require('./iphoneX.js');

let Note8 = new SamSungGalaxyNote8();
let S8 = new SamSungGalaxyS8();
let iphone8 = new Iphone8();
let iphoneX = new IphoneX();


Note8.MobilePhone();
Note8.PhoneCall();
Note8.SMS();
Note8.InternetSurfing();

Note8.UseGearVR();
Note8.TransformToPC();
Note8.UsePen();
Note8.GooglePlay();

S8.MobilePhone();
S8.PhoneCall();
S8.SMS();
S8.InternetSurfing();

S8.UseGearVR();
S8.TransformToPC();
S8.GooglePlay();


iphone8.MobilePhone();
iphone8.PhoneCall();
iphone8.SMS();
iphone8.InternetSurfing();

iphone8.AppStore();
iphone8.TouchID();


iphoneX.MobilePhone();
iphoneX.PhoneCall();
iphoneX.SMS();
iphoneX.InternetSurfing();

iphoneX.AppStore();
iphoneX.FaceID();
