const { MobilePhone } = require('./MobilePhone.js');
class Android extends MobilePhone {
    UseGearVR() {
        console.log('UseGearVR');
    }
    TransformToPC() {
        console.log('TransformToPC');
    }
    GooglePlay() {
        console.log('GooglePlay');
    }
}

module.exports.Android = Android;