const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const render = require('koa-ejs');
const path = require('path');

const app = new Koa();
const router = new Router();

render(app,{
    root:path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache:'false',
    debug:true
});

router.get('/index',async (ctx)=>{
    await ctx.render('index');
});

router.get('/',(ctx)=>{
    ctx.body = `HelloWorld`;
});

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);