// const fs = require('fs');
// const util = require('util');
//const read = util.promisify(fs.readFile);
const readFile = async () => {
    const dataTable = await fetch("../json/homework2_1.json").then(res=>res.json());
    await createTable(dataTable);
}
const createTable = (res) => {
    const head = Object.keys(res[0])
        .reduce((sum, val) => {
            return sum += `<th>${val}</th>`
        }, '');
    const body = res
        .reduce((sum, val) => {
            sum += `<tr>`;
            for (let i in val) {
                sum += `<td>${val[i]}</td>`;
            }
            return sum += `</tr>`;
        }, '');
    const table = `<thead><tr>${head}</tr></thead><tbody>${body}<tbody>`;
    $("#table").append(table);
}
readFile();