const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const render = require('koa-ejs');
const path = require('path');

const app = new Koa();
const router = new Router();

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

router.get('/index', async ctx => {
    const data = { title: 'Index' };
    await ctx.render('index',data);
});

router.get('/skill',async ctx=>{
    const data ={title : 'Skill'};
    await ctx.render('skill',data);
});

router.get('/contact',async ctx=>{
    const data = {title:'Contact ME'};
    await ctx.render('contact',data);
});

router.get('/portfolio',async ctx=>{
    const data ={title:'Portfolio'};
    await ctx.render('portfolio',data);
});

app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);