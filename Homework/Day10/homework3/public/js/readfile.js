const read = async () => {
    const data = await fetch("/json/homework2_1.json").then(res => { return res.json(); });
    table(data);
}
const table = (data) => {
    let head = Object.keys(data[0])
        .reduce((sum, val) => {
            return sum += `<th scope="col">${val}</th>`
        }, ``);
    let body = data
        .reduce((sum, val) => {
            sum+= `<tr>`
            for (let i in val) {
                sum += `<td>${val[i]}</td>`
            }
            return sum+=`</tr>`;
        }, ``);
    let table = `<thead><tr>${head}</tr></thead><tbody>${body}</tbody>`;
    console.log(table);
    $('#table').append(table);
}
read();