fetch("homework2_1.json").then(res => {
    return res.json();
}).then(res => {
    let employees = res;
    addYearSalary(employees[0]);
    addNextSalary(employees[0]);
    addAdditionalFields(employees);
    createTable(employees);
}).catch(err => {
    console.log(err);
});

const addYearSalary = (row) => row.yearSalary = row.salary * 12;
const addNextSalary = (row) => {
    let yearSalary = 3;
    let salaryUp = row.salary;
    let nextSalary = [];
    for (let i = 0; i < yearSalary; i++) {
        nextSalary[i] = salaryUp;
        salaryUp = (salaryUp * .1) + salaryUp;
    }
    row.nextSalary = nextSalary;
    return row;
}

const addAdditionalFields = (row) => {
    for (let i = 0; i < row.length; i++) {
        addYearSalary(row[i]);
        addNextSalary(row[i]);
    }
    return row;
}

const createTable = (row) => {
    let head = ``;
    for (let i in row[0]) {
        head += `<th>${i}</th>`;
    }
    let body = ``;
    for (let i = 0; i < row.length; i++) {
        body += `<tr>`;
        for (let j in row[i]) {
            if (j === "nextSalary") {
                body += `<td><ol>`;
                for (let k = 0; k < row[i][j].length; k++) {
                    body += `<li>${row[i][j][k]}</li>`;
                }
                body += `</ol></td>`
            }
            else {
                body += `<td>${row[i][j]}</td>`;
            }
        }
        body += `</tr>`;
    }
    return table = `<thead><tr>${head}</tr></thead><tbody>${body}</tbody>`;
}

$(function () {
    $("#table").append(table);
});
