import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div>
        <footer class="page-footer font-small blue">
          <div class="footer-copyright text-center py-3">
            © 2018 Copyright:
            <a href="https://mdbootstrap.com/bootstrap-tutorial/">
              {" "}
              MDBootstrap.com
            </a>
          </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
