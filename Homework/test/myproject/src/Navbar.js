import React, { Component } from "react";

class Navbar extends Component {
  render() {
    return (
      <div>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
          <a class="navbar-brand">
            <img src="https://raw.githubusercontent.com/panotza/pikkanode/master/pikkanode.png" alt="logo" width="100px" />
          </a>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link">
              create pikka
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link">
              signup
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link">
              signin
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link">
              signout
              </a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Navbar;
