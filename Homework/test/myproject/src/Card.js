import React from "react";

const Card = props => (
  <div>
    <div class="card" style={{ width: "18rem" }}>
      <img class="card-img-top" src={props.card.imgSrc} alt="Card image cap" />
      <div class="card-body">
        <h5 class="card-title">{props.card.id}</h5>
        <p class="card-text">
          {console.log(props.card)}
          createBy = {props.card.createBy}
          <br />
          date = {props.card.date}
          <br />
          likeCount = {props.card.likeCount}
          <br />
          commentCount = {props.card.commentCount}
          <br />
        </p>
        <a href="#" class="btn btn-primary">
          Go somewhere
        </a>
      </div>
    </div>
  </div>
);

export default Card;
