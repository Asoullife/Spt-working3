import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Hello1 from "./Hello1";
import Hello2 from "./Hello2";
import Navbar from "./Navbar";
import PictureCard from "./PictureCard";
import Footer from "./Footer";

class App extends Component {
  state = {
    data: [
      {
        id: "1",
        imgSrc: "https://i.ytimg.com/vi/x8mpMSw6Wy4/maxresdefault.jpg",
        createBy: "20/9/2020",
        date: "20/9/2020",
        likeCount: "5",
        commentCount: "30"
      },
      {
        id: "2",
        imgSrc: "https://i.ytimg.com/vi/x8mpMSw6Wy4/maxresdefault.jpg",
        createBy: "20/9/2020",
        date: "20/9/2020",
        likeCount: "20",
        commentCount: "300"
      },
      {
        id: "3",
        imgSrc: "https://i.ytimg.com/vi/x8mpMSw6Wy4/maxresdefault.jpg",
        createBy: "20/9/2020",
        date: "20/9/2020",
        likeCount: "8",
        commentCount: "12"
      },
      {
        id: "4",
        imgSrc: "https://i.ytimg.com/vi/x8mpMSw6Wy4/maxresdefault.jpg",
        createBy: "20/9/2020",
        date: "20/9/2020",
        likeCount: "7",
        commentCount: "50"
      },
      {
        id: "5",
        imgSrc: "https://i.ytimg.com/vi/x8mpMSw6Wy4/maxresdefault.jpg",
        createBy: "20/9/2020",
        date: "20/9/2020",
        likeCount: "70",
        commentCount: "90"
      }
    ]
  };
  render() {
    return (
      <div>
        {/* <Hello1 />
        <Hello2 /> */}
        <Navbar />
        <PictureCard cardData={this.state.data} />
        <Footer />
      </div>
    );
  }
}

export default App;
