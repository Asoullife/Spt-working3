import React, { Component } from "react";
import Card from './Card';

class PictureCard extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
          {this.props.cardData.map(data=>
        <Card key={data.id} card = {data}/>)}
      </div>
    );
  }
}

export default PictureCard;
