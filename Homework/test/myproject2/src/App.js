import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import GenerateUser from './GenerateUser';

class App extends Component {

  render() {
    return (
      <div>
        <GenerateUser />
      </div>
    );
  }
}

export default App;
