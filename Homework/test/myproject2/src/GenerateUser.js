import React,{Component} from 'react';

class GenerateUser extends Component{
    state={
        nextId:1,
        data:[]
    }
    handleGetdata = async ()=>{
        const {results} = await fetch("https://randomuser.me/api/").then(res=>res.json());
        console.log(results)
        console.log(this.state.data)
        this.setState({ data: [...this.state.data, {
            id:this.state.nextId++,
            email:results[0].email,
            gender:results[0].gender,
            name:{
                title:results[0].name.title, 
                first:results[0].name.first, 
                last:results[0].name.last
            },
            picture:results[0].picture.large
        }]})
    }
    render(){
        return(
            <div>
                {this.state.data.map(data=>
                    <div key={data.id}>
                        <img src={data.picture} />
                        <p>email:{data.email}</p>
                        <p>gender:{data.gender}</p>
                        <p>name:{`${data.name.title} ${data.name.firse} ${data.name.last}`}</p>
                    </div>
                )}
                <button onClick={this.handleGetdata}>GenerateUser</button>
            </div>
        )
    }
}

export default GenerateUser;