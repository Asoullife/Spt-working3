const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path');
const render = require('koa-ejs');
const mysql = require('mysql2/promise');

const app = new Koa();
const router = new Router();

render(app,{
    root:path.join(__dirname,'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false,
    debug:true
});

router.get('/',async (ctx,next)=>{
    const pool = await mysql.createPool({
        connectionLimit:10,
        host:'localhost',
        user:'root',
        password:'',
        database:'online_course'
    });
    const [rows,fields] = await pool.query(`select instructors.name as Instructors_Name,courses.name as Courses_Name
                                            from courses
                                            right join instructors on courses.teach_by = instructors.id
                                            where courses.id is null
                                            order by courses.id`);
    const [rows2] = await pool.query(`SELECT courses.name as Courses_Name,instructors.name as Instructors_Name
                                    FROM courses
                                    LEFT JOIN instructors on courses.teach_by = instructors.id
                                    WHERE instructors.id is NULL
                                    ORDER BY instructors.id;`);
    await ctx.render('index',{rows,rows2});
});

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);