import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import UserGenerator from './UserGenerator';

class App extends Component {
  render() {
    return (
      <div className="App">
        
        <UserGenerator />
      </div>
    );
  }
}

export default App;
