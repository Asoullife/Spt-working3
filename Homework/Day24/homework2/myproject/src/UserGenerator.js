import React from 'react';

class UserGenerator extends React.Component{
    state = {
        img:'',
        email:'',
        gender:'',
        name:'',
    }
    getUser = async(e)=> {
        const result = await fetch("https://randomuser.me/api/").then(res=>res.json());
        const data = result.results[0];
        this.setState({
             img: data.picture.large,
             email: data.email,
             gender: data.gender,
             name: data.name
        });
    }
    render(){
        return(
            <div>
                <img src={this.state.img} alt="pic" />
                <p>email:{this.state.email}</p>
                <p>gender:{this.state.gender}</p>
                <p>{this.state.name.title} {this.state.name.first} {this.state.name.last}</p>
                <button onClick={this.getUser}>Generator</button>
            </div>
        )
    }
}

export default UserGenerator;