import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import CardDeck from './AppCardDeck';
import './AppCardDeck.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          <CardDeck />
        </p>
      </div>
    );
  }
}

export default App;
