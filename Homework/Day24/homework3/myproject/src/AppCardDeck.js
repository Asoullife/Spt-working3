import React from 'react';

class CardDeck extends React.Component{
    state = {
        colors:''
    }
    shuffleCard = (e)=>{
        const shuffle = ([...arr]) => {
            let m = arr.length;
            while (m) {
              const i = Math.floor(Math.random() * m--);
              [arr[m], arr[i]] = [arr[i], arr[m]];
            }
            return arr;
          };
        const cardColor = ['red', 'blue', 'green', 'purple', 'pink'];
        const card = shuffle(cardColor);
        this.setState({
            colors : card
        });
    }
    view = (color)=>{
        alert(`you Click: ${color}`);
    }
    render(){
        return(
            <div>
                <div className="container">
                    <div className="cardColor" onClick={(Click)=>{this.view(this.state.colors[0])}} style={{backgroundColor:this.state.colors[0]}}>

                    </div>
                    <div className="cardColor" onClick={(Click)=>{this.view(this.state.colors[1])}} style={{backgroundColor:this.state.colors[1]}}>

                    </div>
                    <div className="cardColor" onClick={(Click)=>{this.view(this.state.colors[2])}} style={{backgroundColor:this.state.colors[2]}}>

                    </div>
                    <div className="cardColor" onClick={(Click)=>{this.view(this.state.colors[3])}} style={{backgroundColor:this.state.colors[3]}}>

                    </div>
                    <div className="cardColor" onClick={(Click)=>{this.view(this.state.colors[4])}} style={{backgroundColor:this.state.colors[4]}}>

                    </div>
                </div>
                <button onClick={this.shuffleCard}>Gen</button>
            </div>
        )
    }
}

export default CardDeck;