import React from 'react';
import PictureCard from './PictureCard';

class Pikka extends React.Component{
    render(){
        const date = new Date();
        return(
            <div>
                <PictureCard
                id ={1}
                imgSrc ={"http://sfwallpaper.com/images/background-hd-photo-30.jpg"}
                createBy={'abc'}
                date={`${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`}
                likeCount={100}
                commentCount={20}
                />
                <PictureCard
                id ={2}
                imgSrc ={"http://www.3rdcavalrydivision.com/wp-content/uploads/2018/06/loop-background-8.jpg"}
                createBy={'abc'}
                date={`${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`}
                likeCount={50}
                commentCount={30}
                />
                <PictureCard
                id ={3}
                imgSrc ={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsHzNoxY1Ekyi1aNvglVX_0P5XtXDa42AnR_c7kgZ-M765wr79Ug"}
                createBy={'abc'}
                date={`${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`}
                likeCount={60}
                commentCount={25}
                />
                <PictureCard
                id ={4}
                imgSrc ={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReDcMzFCle60d4STNaVMxVHgoVZZw1o5s-xT8m84v4of2XRnqm"}
                createBy={'abc'}
                date={`${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`}
                likeCount={15}
                commentCount={0}
                />
                <PictureCard
                id ={5}
                imgSrc ={"http://sfwallpaper.com/images/anime-background-images-1.jpg"}
                createBy={'abc'}
                date={`${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`}
                likeCount={500}
                commentCount={60}
                />
            </div>
        )
    }
}

export default Pikka;