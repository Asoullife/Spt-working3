import React from 'react';
import './PictureCard.css';

class PictureCard extends React.Component{
    render(){
        return (
            <div>
                <img src={this.props.imgSrc} alt="picCard" className="img" />
                <div className="p">
                    <p>id:{this.props.id}</p>
                    <p>createBy:{this.props.createBy}</p>
                    <p>date:{this.props.date.toString()}</p>
                    <p>like:{this.props.likeCount}</p>
                    <p>comment:{this.props.commentCount}</p>
                </div>
            </div>
        )
    }
}

export default PictureCard;