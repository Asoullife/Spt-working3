const pool = require("../db");

const register = async (email, password) => {
  try {
    const result = await pool.query(
      `
    insert into users
        (email,password)
    values
        (?,?)`,
      [email, password]
    );
    return result[0].insertId;
  } catch (e) {
    return e;
  }
};
const login = async (email) => {
  try {
    const [result] = await pool.query(`
        select id,email,password
        from users
        where email = ?`,[email]);
    return result;
  } catch (e) {
    console.log("aaaa");
  }
};

module.exports = {
  register,
  login
};
