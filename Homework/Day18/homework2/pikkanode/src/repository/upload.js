const pool = require('../db');

const picture = async(id,caption,created_by)=>{
    const result = await pool.query(`
    insert into pictures
        (id,caption,created_by)
    values
        (?,?,?)`,[id,caption,created_by]);
    return result[0].insertId;
}

module.exports = {
    picture
}