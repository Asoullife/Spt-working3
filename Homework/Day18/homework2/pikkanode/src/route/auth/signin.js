const {user} = require('../../repository');
const bcrypt = require('bcrypt');
const getHendler = async(ctx,next)=>{
    if(ctx.session.userId){return ctx.redirect('/')};
    const data = {
        flash:ctx.flash
    }
    await ctx.render('signin',data);
    await next();
}
const postHendler = async(ctx)=>{
    const {email,password} = ctx.request.body;
    if(!email || !password){
        ctx.session.flash = {error:'email && password require'};
        return ctx.redirect('/signin');
    }
    const userDb = await user.login(email);
    const same = await bcrypt.compare(password,userDb[0].password);
    if(!same){
        ctx.session.flash = {error:'wrong password'};
        return ctx.redirect('/signin');
    }
    ctx.session.userId = userDb[0].id;
    ctx.redirect('/');
}

const checkAuth = async (ctx, next) => {
    if (!ctx.session || !ctx.session.userId) {
      ctx.body = `<p>you are not signed in</p>
        <a href="/signin">signin here</a>
      `
      return
    }
    ctx.body = 'your are signin';
    await next();
  }

module.exports = {
    getHendler,
    postHendler,
    checkAuth
}