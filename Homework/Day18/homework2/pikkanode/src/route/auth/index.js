const Router = require('koa-router');
const signin = require('./signin');
const signup = require('./signup');

const router = new Router();


router.get('/',signin.checkAuth);
router.get('/signin',signin.getHendler);
router.post('/signin',signin.postHendler);
router.get('/signup',signup.getHendler);
router.post('/signup',signup.postHendler);

module.exports = router.routes();