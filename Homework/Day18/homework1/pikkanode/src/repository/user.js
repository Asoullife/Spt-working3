const pool = require("../db");

const register = async (email, password) => {
  const result = await pool.query(
    `
    insert into users
        (email,password)
    values
        (?,?)`,
    [email, password]
  );
  return result[0].insertId;
};
const login = async email => {
  const [result] = await pool.query(
    `
        select id,email,password
        from users
        where email = ?`,
    [email]
  );
  return result;
};

module.exports = {
  register,
  login
};
