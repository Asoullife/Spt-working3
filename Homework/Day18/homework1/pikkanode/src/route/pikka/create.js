const uuidv4 = require("uuid/v4");
const fs = require("fs-extra");
const path = require("path");
const { upload } = require("../../repository");

const getHendler = async ctx => {
  const data = {
    flash: ctx.flash
  };
  await ctx.render("/upload", data);
};

const pictureDir = path.join(process.cwd(), "public", "images");
const allowFileType = {
  "image/jpg": true,
  "image/png": true,
  "image/jpeg": true
};

const postHendler = async ctx => {
  try {
    if (!allowFileType[ctx.request.files.pic.type]) {
      throw new Error("file type not allow");
    }
    const caption = ctx.request.body.caption;
    const filename = uuidv4();
    await fs.rename(ctx.request.files.pic.path,path.join(pictureDir, filename));
    const created_by = 1;
    const picId = await upload.picture(filename, caption, created_by);
    ctx.redirect("/");
  } catch (e) {
    fs.remove(ctx.request.files.pic.path);
    ctx.session.flash = { error: e };
    ctx.redirect("/upload");
  }
};

module.exports = {
  getHendler,
  postHendler
};
