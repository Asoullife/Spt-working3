const Router = require('koa-router');
const create = require('./create');
const router = new Router();

router.get('/upload',create.getHendler);
router.post('/upload',create.postHendler);

module.exports = router.routes();