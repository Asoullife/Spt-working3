const Koa = require('koa');
const serve = require('koa-static');
const render = require('koa-ejs');
const path = require('path');
const koabody = require('koa-body');
const session = require('koa-session');

const app = new Koa();

render(app,{
    root:path.join(process.cwd(),'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
});

const flash = async(ctx,next)=>{
    if(!ctx.session){
        throw new Error('flash message');
    }
    ctx.flash = ctx.session.flash;
    delete ctx.session.flash;
    await next();
}

const sessionStore = {};
const sessionConfig = {
    key:'sess',
    maxAge:1000*60*60,
    httpOnly:true,
    store:{
        get(key,maxAge,{rolling}){
            return sessionStore[key];
        },
        set(key,sess,maxAge,{rolling}){
            sessionStore[key]=sess;
        },
        destroy(key){
            delete sessionStore[key];
        }
    }
}

app.keys = ['supersess'];
app.use(session(sessionConfig,app));
app.use(flash);
app.use(koabody({multipart:true}));
app.use(require('./route'));
app.use(serve(path.join(process.cwd(),'public')));

app.listen(8000);