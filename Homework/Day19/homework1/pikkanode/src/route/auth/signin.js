const {user} = require('../../repository');
const bcrypt = require('bcrypt');

const getHendler = async (ctx,next)=>{
    if(ctx.session.userId){return ctx.redirect('/')}
    const data = {
        flash:ctx.flash
    };
    await ctx.render('signin',data);
}
const postHendler = async(ctx,next)=>{
    const {email,password} = ctx.request.body;
    if(!email || !password){
        ctx.session.flash = {error: 'email && password require'};
        return ctx.redirect('/signin');
    }
    const userDb = await user.login(ctx,email);
    if(userDb[0] === undefined){
        ctx.session.flash = {error:'wrong email && password'};
        return ctx.redirect('/signin');
    }
    const same = await bcrypt.compare(password,userDb[0].password);
    if(!same){
        ctx.session.flash = {error:'wrong email && password'};
        return ctx.redirect('/signin');
    }
    ctx.session.userId = userDb[0].id;
    ctx.redirect('/');
}

const checkauth = async(ctx,next)=>{
    if(!ctx.session || !ctx.session.userId){
        ctx.body = `<p>your are not signin</p>
        <a href="/signin">Signin here</a>`;
        return;
    }
    ctx.body = `your are signin`;
    await next();
}

module.exports ={
    getHendler,
    postHendler,
    checkauth
}