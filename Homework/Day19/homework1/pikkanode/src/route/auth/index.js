const Router = require('koa-router');
const signin = require('./signin');
const signup = require('./signup');
const signout = require('./signout');
const router = new Router();

router.get('/',signin.checkauth);
router.get('/signout',signout.getHendler);
router.post('/signout',signout.postHendler);
router.get('/signin',signin.getHendler);
router.post('/signin',signin.postHendler);
router.get('/signup',signup.getHendler);
router.post('/signup',signup.postHendler);

module.exports = router.routes();