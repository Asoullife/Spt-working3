const {upload} = require('../../repository');
const uuidv4 = require('uuid/v4');
const path = require('path');
const fs = require('fs-extra');

const getHendler = async(ctx,next)=>{
    if(!ctx.session.userId){return ctx.redirect('/')}
    const data = {
        flash:ctx.flash
    };
    await ctx.render('/upload',data);
}

const pictureDir = path.join(process.cwd(),'public','images')
const allowFileType ={
    'image/jpg':true,
    'image/png':true,
    'image/jpeg':true
}

const postHendler = async(ctx,next)=>{
    try {
        if(!allowFileType[ctx.request.files.pic.type]){
            throw new Error('File type Not Allow');
        }
        const caption = ctx.request.body.caption;
        const filename = uuidv4();
        await fs.rename(ctx.request.files.pic.path,path.join(pictureDir,filename));
        const created_by = ctx.session.userId;
        const userId = await upload.picture(filename,caption,created_by);
        ctx.redirect('/');
    } catch (e) {
        ctx.session.flash = {error:e};
        await fs.remove(ctx.request.files.pic.path);
        ctx.redirect('/upload');
    }
}

module.exports = {
    getHendler,
    postHendler
}