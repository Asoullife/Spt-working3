const { picture } = require("../../../../repository");
const uuidv4 = require("uuid/v4");
const path = require("path");
const fs = require("fs-extra");

const allowFileType = {
  "image/png": true,
  "image/jpeg": true
};

const pictureDir = path.join(process.cwd(), "public", "images");

const postAPIHandler = async ctx => {
  try {
    const { caption } = ctx.request.body;
    if (!ctx.request.files.picture) {
      ctx.status = 400;
      ctx.body = {error: `picture required`};
      return;
    }
    if (!allowFileType[ctx.request.files.picture.type]) {
      ctx.status = 400;
      ctx.body = {error: `not accept upload file type`};
      return;
    }
    if (!caption) {
      ctx.status = 400;
      ctx.body = {error: `caption required`};
      return;
    }
    const generatedName = uuidv4();
    await fs.rename(ctx.request.files.picture.path,path.join(pictureDir, generatedName));
    const result = await picture.uploadPicture(ctx,generatedName,caption,ctx.session.userId);
    if (!result) {
      ctx.status = 400;
      ctx.body = {error: `system error`};
    } else {
      const queryResult = await picture.retrievePictureByIdAPI(ctx, result);
      ctx.status = 200;
      ctx.body = {id: result,createdAt: queryResult.uploaded_at};
    }
    return;
  } catch (err) {
    console.log(err);
    fs.remove(ctx.request.files.picture.path);
    ctx.status = 400;
    ctx.body = {error: `system error`};
    return;
  }
};

module.exports = {
  postAPIHandler
};
