const Router = require('koa-router')

const signIn = require('./signin')
const signUp = require('./signup')
const signOut = require('./signout')

const router = new Router()

router.prefix('/api/v1')
router.post('/auth/signup', signUp.postAPIHandler)
router.post('/auth/signin', signIn.postApiHandler)
router.post('/auth/signout', signOut.postApiHandler)

module.exports = router.routes()