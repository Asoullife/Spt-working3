const pool = require("../db");
const mysqlErrors = require("mysql2/lib/constants/errors");

const register = async (ctx, email, password) => {
  try {
    const result = await pool.query(
      `
            insert into users
                (reg_email,password)
            values
                (?,?)`,
      [email, password]
    );
    return result[0].insertId;
  } catch (e) {
    switch (e.errno) {
      case mysqlErrors.ER_DUP_ENTRY:
        ctx.status = 400;
        ctx.session.flash = { error: "Email is registered" };
        break;
      default:
        ctx.status = 500;
        ctx.session.flash = { error: "Server Error" };
        break;
    }
  }
};

const login = async (ctx, email) => {
  try {
    const result = await pool.query(
      `
    select id,password
    from users
    where reg_email = ?`,
      [email]
    );
    return result[0];
  } catch (e) {
    switch (e.errno) {
      default:
        ctx.status = 500;
        ctx.session.flash = { error: "Server Error" };
        break;
    }
  }
};

module.exports = {
  register,
  login
};
