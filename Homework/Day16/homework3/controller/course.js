const mysql = require("mysql2/promise");

const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "design_pattern"
});

const model = require("../models/course")(pool);

const findCourseId = async (ctx, next) => {
  const id = await model.courseId(ctx.params.id);
  await ctx.render("table", {
    user: id
  });
  await next();
};
const findCoursePrice = async (ctx, next) => {
  const price = await model.coursePrice(ctx.params.price);
  await ctx.render("table", {
    user:price
  });
  await next();
};

module.exports = { findCourseId, findCoursePrice };
