class User {
    constructor(pool, rows) {
      this.id = rows.id;
      this.name = rows.name;
      this.created_at = rows.created_at;
      //this.jsonData = JSON.parse(rows.json_data);
    }
    setPool(pool) {
      this._pool = pool;
    }
  }

module.exports = function(pool) {
  return {
    async courseId(id) {
      const [rows] = await pool.query("select*from courses where id=?", [id]);
      return rows.map(rows => new User(pool, rows));
    },

    async coursePrice(price) {
      const [rows] = await pool.query("select*from courses where price=?", [
        price
      ]);
      return rows.map(rows => new User(pool, rows));
    }
  };
};
