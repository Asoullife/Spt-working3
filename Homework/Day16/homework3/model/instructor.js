class User {
  constructor(pool, rows) {
    this.id = rows.id;
    this.name = rows.name;
    this.created_at = rows.created_at;
    //this.jsonData = JSON.parse(rows.json_data);
  }
  setPool(pool) {
    this._pool = pool;
  }
}

module.exports = function(pool) {
  return {
    async findAll() {
      const [rows] = await pool.query("select*from instructors");
      return rows.map(rows => new User(pool, rows));
    },

    async findId(id) {
      const [rows] = await pool.query("select*from instructors where id=?", [
        id
      ]);
      return rows.map(rows => new User(pool, rows));
    }
  };
};
