const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path');
const render = require('koa-ejs');

const app = new Koa();
const router = new Router();

render(app,{
    root:path.join(__dirname,'views'),
    layout: false,
    viewExt: 'ejs',
    cache: false,
    debug: true
});

router.get('/instructor/find_all',require('./controllers/instructor').findAll);
router.get('/instructor/find_by_id/:id',require('./controllers/instructor').findId);
router.get('/course/find_by_id/:id',require('./controller/course').findCourseId);
router.get('/course/find_by_price/:price',require('./controller/course').findCoursePrice);

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);