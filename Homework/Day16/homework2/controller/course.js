const model = require("../models/course");

const findCourseId = async (ctx, next) => {
  const id = await model.courseId(ctx.params.id);
  await ctx.render("table", {
    user: id
  });
  await next();
};
const findCoursePrice = async (ctx, next) => {
  const price = await model.coursePrice(ctx.params.price);
  await ctx.render("table", {
    user:price
  });
  await next();
};

module.exports = { findCourseId, findCoursePrice };
