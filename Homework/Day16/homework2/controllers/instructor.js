const model = require("../model/instructor");

const findAll = async (ctx, next) => {
  const find_All = await model.findAll();
  await ctx.render("table", {
    user: find_All
  });
  await next();
};

const findId = async (ctx, next) => {
  const find_id = await model.findId(ctx.params.id);
  await ctx.render("table", {
    user: find_id
  });
  await next();
};

module.exports = { findAll, findId };
