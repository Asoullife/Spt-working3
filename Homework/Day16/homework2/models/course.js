const mysql = require("mysql2/promise");

const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "design_pattern"
});

const createEntity = rows => {
  if (!rows.id) {
    return {};
  }
  return {
    id: rows.id,
    name: rows.name,
    detail: rows.detail,
    price: rows.price,
    teach_by: rows.teach_by,
    ctrated_at: rows.created_at
  };
};

const courseId = async id => {
  const [rows] = await pool.query("select*from courses where id=?", [id]);
  return rows.map(val => createEntity(val));
};

const coursePrice = async price => {
  const [rows] = await pool.query("select*from courses where price=?", [price]);
  return rows.map(val=>createEntity(val));
};

module.exports = { courseId, coursePrice };
