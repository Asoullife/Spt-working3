const mysql = require("mysql2/promise");

const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "design_pattern"
});

const createEntity = rows => {
  if (!rows.id) {
    return {};
  }
  return {
    id: rows.id,
    name: rows.name,
    created_at: rows.created_at
  };
};

const findAll = async () => {
  const [rows] = await pool.query('select*from instructors');
  return rows.map(val => createEntity(val));
};

const findId = async(id)=>{
    const [rows] = await pool.query('select*from instructors where id=?',[id]);
    return rows.map(val => createEntity(val));
}

module.exports = { findAll,findId };
