const Koa = require('koa');
const serve = require('koa-static');
const path = require('path');
const koaBody = require('koa-body');
const render = require('koa-ejs');
const session = require('koa-session');
const cors = require('@koa/cors');

const app = new Koa();

render(app,{
    root: path.join(process.cwd(),'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
});

const flash = async(ctx,next)=>{
    if(!ctx.session){
        throw new Error('flash');
    }
    ctx.flash = ctx.session.flash;
    delete ctx.session.flash;
    await next();
}

const sessionStore = {};
const sessionConfig = {
    key:'sess',
    maxAge:1000*60*60,
    httpOnly:true,
    store:{
        get(key,maxAge,{rolling}){
            return sessionStore[key];
        },
        set(key,sess,maxAge,{rolling}){
            sessionStore[key] = sess;
        },
        destroy(key){
            delete sessionStore[key];
        }
    }
}

const corsConfig = {
    origin: 'https://panotza.github.io',
    allowMethods:['GET','POST','PUT','DELETE'],
    allowHeaders:['Authorization','Content-Type'],
    maxAge:10,
    credentials: true
}

const stripPrefix = async (ctx, next) => {
    if (!ctx.path.startsWith('/-')) {
      ctx.status = 404
      return
    }
  
    ctx.path = ctx.path.slice(2)
    await next()
  }

app.keys = ['super'];
app.use(cors(corsConfig));
app.use(koaBody({multipart:true}));
app.use(session(sessionConfig,app));
app.use(flash);
app.use(require('./route'));
app.use(stripPrefix);
app.use(serve(path.join(process.cwd(),'public')));

app.listen(8000);