const pool = require("../db");

const picture = async (id, caption, created_by) => {
  try {
    const result = await pool.query(
      `
    insert into pictures
        (picture_id,caption,uploaded_user)
    values
        (?,?,?)`,
      [id, caption, created_by]
    );
    return result;
  } catch (err) {
    switch (err.errno) {
      default:
        ctx.status = 500;
        ctx.session.flash = { error: "Server Error" };
        break;
    }
  }
};

module.exports = {
  picture
};
