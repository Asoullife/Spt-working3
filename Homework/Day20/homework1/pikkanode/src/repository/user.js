const pool = require("../db");
const mysqlErrors = require("mysql2/lib/constants/errors");

const register = async (ctx, email, password) => {
  try {
    const result = await pool.query(
      `
            insert into users
                (reg_email,password)
            values
                (?,?)`,
      [email, password]
    );
    return result[0].insertId;
  } catch (e) {
    switch (e.errno) {
      case mysqlErrors.ER_DUP_ENTRY:
        ctx.status = 400;
        ctx.session.flash = { error: "Email is registered" };
        break;
      default:
        ctx.status = 500;
        ctx.session.flash = { error: "Server Error" };
        break;
    }
  }
};

const login = async (ctx, email) => {
  try {
    const result = await pool.query(
      `
    select id,password
    from users
    where reg_email = ?`,
      [email]
    );
    return result[0];
  } catch (e) {
    switch (e.errno) {
      default:
        ctx.status = 500;
        ctx.session.flash = { error: "Server Error" };
        break;
    }
  }
};

const fbRegister = async (ctx, fb_id, firstName, lastName, email) => {
	try {
		const result = await pool.query(`
			INSERT INTO users
				(fb_id, firstname, lastname, reg_email)
			VALUES (?, ?, ?, ?)`, 
				[fb_id, firstName, lastName, email])
		ctx.session.flash = { success: 'Register success' }
		return result[0].insertId
	} catch (err) {
		switch (err.errno) {
			case mysqlErrors.ER_DUP_ENTRY:
				ctx.status = 400
				ctx.session.flash = { error: 'Email is registered' }
				break
			default:
				ctx.status = 500
				ctx.session.flash = { error: 'Server Error' }
				break
		}
	}
}

const updateFBProfile = async (ctx, fb_id, firstname, lastname, email) => {
	try {
		const [result] = await pool.query(`
			UPDATE users
			SET fb_id = ?, firstname = ?, lastname = ?
			WHERE reg_email = ?`,
				[fb_id, firstname, lastname, email])
		console.log(result)
		return result[0]
	} catch (err) {
		switch (err.errno) {
			default:
				ctx.status = 500
				ctx.session.flash = { error: 'Server Error' }
			break
		}
	}
}

module.exports = {
  register,
  login,
  fbRegister,
  updateFBProfile
};
