const Router = require('koa-router');
const upload = require('./create');
const router = new Router();

router.get('/create',upload.getHendler);
router.post('/create',upload.postHendler);

module.exports = router.routes();