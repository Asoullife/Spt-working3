const { user } = require("../../../../repository");
const bcrypt = require("bcrypt");

const postApiHandler = async ctx => {
  const { email, password } = ctx.request.body;
  const result = await user.login(ctx,email);
  if (!result) {
    ctx.status = 400;
    ctx.body = {error: `wrong email or password`};
    return ctx;
  }
  const checkPassword = await bcrypt.compare(password, result[0].password);
  if (!checkPassword) {
    ctx.status = 400;
    ctx.body = {error: `wrong email or password`};
    return ctx;
  } 
  else {
    ctx.status = 200;
    ctx.body = {};
    ctx.session.userId = result[0].id;
    return ctx;
  }
};

module.exports = {
  postApiHandler
};
