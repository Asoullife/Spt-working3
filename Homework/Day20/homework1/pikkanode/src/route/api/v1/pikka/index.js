const Router = require('koa-router')

const create = require('./create')

const router = new Router()

const checkAuth = async (ctx, next) => {
    if (!ctx.session || !ctx.session.userId) {
        ctx.status = 401
        ctx.body = {error: `unauthorized`}
        return
    }
    await next()
}

router.prefix('/api/v1')
router.post('/pikka', checkAuth, create.postAPIHandler)

module.exports = router.routes()