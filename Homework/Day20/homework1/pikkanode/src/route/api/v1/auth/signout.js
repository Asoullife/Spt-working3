const postApiHandler = async ctx => {
    ctx.session = null
    ctx.status = 200
    ctx.body = {}
}

module.exports = {
    postApiHandler
}