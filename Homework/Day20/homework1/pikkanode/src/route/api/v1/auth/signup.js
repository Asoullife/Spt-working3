const { user } = require('../../../../repository')
const bcrypt = require('bcrypt')

const emailRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;

const postAPIHandler = async ctx => {
    const {email,password} = ctx.request.body
    if (!emailRegex.test(email)) {
        ctx.status = 400
        ctx.body =  {error: `invalid email`}
        return ctx
    }
    if (password.length < 6) {
        ctx.status = 400
        ctx.body =  {error: `password too short`}
        return ctx
    }
    const hashpassword = await bcrypt.hash(password,10);
    const userId = await user.register(ctx,email,hashpassword);
    if (!userId) {
        ctx.status = 400
        ctx.body = {error: `email already used`}
    } else {
        ctx.status = 200
        ctx.body =  {userId: userId}
    }
    return ctx
}

module.exports = {
    postAPIHandler
}