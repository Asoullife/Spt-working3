const { user } = require("../../repository");
const bcrypt = require("bcrypt");

const getHendler = async (ctx, next) => {
  if (ctx.session.userId) {
    return ctx.redirect("/");
  }
  const data = {
    flash: ctx.flash
  };
  await ctx.render("signin", data);
};
const postHendler = async (ctx, next) => {
  const { fb_id, firstName, lastName, email, password } = ctx.request.body;
  if (fb_id) {
    const result = await user.login(ctx, email);
    if (result) {
      if (result.fb_id) {
        ctx.session.userId = result.fb_id;
        ctx.session.userName = result.reg_email;
        return ctx.redirect("/main");
      } else {
        await user.updateFBProfile(ctx, fb_id, firstName, lastName, email);
        const updateResult = await user.login(ctx, email);
        ctx.session.userId = updateResult.fb_id;
        ctx.session.userName = updateResult.reg_email;
        return ctx.redirect("/main");
      }
    } else {
      await user.fbRegister(ctx, fb_id, firstName, lastName, email);
      const result = await user.login(ctx, email);
      ctx.session.userId = result.fb_id;
      ctx.session.userName = result.reg_email;
      return ctx.redirect("/main");
    }
  } else {
    const result = await user.login(ctx, email);
    if (!result.password) {
      ctx.session.flash = { error: "Please login with Facebook" };
      return ctx.redirect("/signin");
    }
    if (!email && !password) {
      ctx.session.flash = { error: "Please enter the credential" };
      return ctx.redirect("/signin");
    }
    const checkPassword = await bcrypt.compare(password, result.password);
    if (!checkPassword) {
      ctx.session.flash = { error: "Entered credential is incorrect" };
      return ctx.redirect("/signin");
    } else {
      ctx.session.userId = result.fb_id ? result.fb_id : result.id;
      ctx.session.userName = result.reg_email;
      return ctx.redirect("/main");
    }
  }
};

const checkauth = async (ctx, next) => {
  if (!ctx.session || !ctx.session.userId) {
    ctx.body = `<p>your are not signin</p>
        <a href="/signin">Signin here</a>`;
    return;
  }
  ctx.body = `your are signin`;
  await next();
};

module.exports = {
  getHendler,
  postHendler,
  checkauth
};
