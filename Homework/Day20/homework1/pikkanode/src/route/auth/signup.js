const {user} = require('../../repository');
const bcrypt = require('bcrypt');

const getHendler = async(ctx)=>{
    if(ctx.session.userId){return ctx.redirect('/')}
    const data = {
        flash:ctx.flash
    };
    await ctx.render('signup',data);
}
const postHendler = async(ctx)=>{
    const {email,password,confirm_password} = ctx.request.body;
    if(password !== confirm_password){
        ctx.session.flash = {error:'wrong password'};
        return ctx.redirect('/signin');
    }
    const hashpassword = await bcrypt.hash(password,10);
    const userId = await user.register(ctx,email,hashpassword);
    ctx.session.userId = userId;
    ctx.redirect('/');
}

module.exports = {
    getHendler,
    postHendler
}