const getHendler = async(ctx)=>{
    await ctx.render('signout');
}
const postHendler = async(ctx)=>{
    ctx.session = null;
    ctx.redirect('/signin');
}

module.exports = {
    getHendler,
    postHendler
}