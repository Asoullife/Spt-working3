const fs = require('fs');

let robot = "";
fs.readFile('head.txt','utf8',readHead);
function readHead(err,data){
    if(err){
        console.log(err);
        return;
    }
    robot += `${data}\n`;
    fs.readFile('body.txt','utf8',readBody);
}
function readBody(err,data){
    if(err){
        console.log(err);
        return;
    }
    robot += `${data}\n`;
    fs.readFile('leg.txt','utf8',readLeg);
}
function readLeg(err,data){
    if(err){
        console.log(err);
        return;
    }
    robot += `${data}\n`;
    fs.readFile('feet.txt','utf8',readFeet);
}
function readFeet(err,data){
    if(err){
        console.log(err);
        return;
    }
    robot += `${data}\n`;
    fs.writeFile('robot.txt',robot,'utf8',writeRobot);
}
function writeRobot(err){
    if(err){
        console.log(err);
        return;
    }
}