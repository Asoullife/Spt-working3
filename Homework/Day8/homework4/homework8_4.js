function myWait(value, callback) {
  setTimeout(function(param){
    console.log(param);
    callback(value);
  }, 1000, value);
}

let resultArr = [];
function onCompleted(value) {
  resultArr = value.map(val=>val*2);
  console.log(resultArr);
}

function promiseAll(arr) {
  //for (let i = 0; i<arr.length; i++) {
    myWait(arr, onCompleted);
  //}
}

promiseAll([5,6,7]);

// function myWait(value, callback) {
//   setTimeout(function (param) {
//     console.log(param);
//     callback(value);
//   }, 1000, value);
// }

// let resultArr = [];
// let count = 0;
// function onCompleted(value) {
//   resultArr[count] = value * 2;
//   count++;
//   if (count === 3) {
//     console.log(resultArr);
//   }
// }

// function promiseAll(arr) {
//   for (let i = 0; i < arr.length; i++) {
//     myWait(arr[i], onCompleted);
//   }
// }

// promiseAll([5, 6, 7]);