function myWait(value, length, callback) {
  setTimeout(function(param, length){
    console.log(param);
    callback(param, length);
  }, 1000, value, length);
}

let resultArr = [];
function onCompleted(value, length) {
  resultArr.push(value);
  
  if(resultArr.length == length) {
    console.log(resultArr);
  }
}

function promiseAll(arr) {
  for (let i = 0; i<arr.length; i++) {
    myWait(arr[i] * 2, arr.length, onCompleted);
  }
}

promiseAll([5,6,7]);