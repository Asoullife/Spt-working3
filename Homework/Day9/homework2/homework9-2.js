const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const head = readFile('head.txt','utf8');
const body = readFile('body.txt','utf8');
const leg = readFile('leg.txt','utf8');
const feet = readFile('feet.txt','utf8');

Promise.all([head,body,leg,feet]).then(res=>{
    let data = '';
    for(let i =0;i<res.length;i++){
        data += `${res[i]}\n`;
    }
    writeFile('robot.txt',data,'utf8');
}).catch(err=>{
    console.log(err);
});