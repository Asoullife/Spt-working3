'use strict'

const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile); 
const readWrite = async ()=>{
    try {
        const head = await readFile('head.txt','utf8');
        const body = await readFile('body.txt','utf8');
        const leg = await readFile('leg.txt','utf8');
        const feet = await readFile('feet.txt','utf8');
        const all = `${head}\n${body}\n${leg}\n${feet}`
        await writeFile('robot.txt',all,'utf8');
    } catch (error) {
        console.log(error);
    }
}
readWrite();