'use strict'
const fs = require('fs');
const util = require('util');

const read = util.promisify(fs.readFile);             //ทำเหมือน ข้อ9_2 แต่ไม่ต้องไป new promise เอง
const write = util.promisify(fs.writeFile);

const readWrite = async ()=>{
    try {
        const head = await read('head.txt','utf8');
        const body = await read('body.txt','utf8');
        const leg = await read('leg.txt','utf8');
        const feet = await read('feet.txt','utf8');
        const allData = `${head}\n${body}\n${leg}\n${feet}\n`;
        await write('robot.txt',allData,'utf8');
        } catch (error) {
        console.error(error);
    }
}

readWrite();