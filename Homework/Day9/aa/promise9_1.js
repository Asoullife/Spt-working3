'use strict'

let fs = require('fs');

const readhead = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('head.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(`${data}\n`);
            }
        });
    });
}
const readbody = (robot) => {
    return new Promise((resolve, reject) => {
        fs.readFile('body.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(robot += `${data}\n`);
            }
        });
    });
}
const readleg = (robot) => {
    return new Promise((resolve, reject) => {
        fs.readFile('leg.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(robot += `${data}\n`);
            }
        });
    });
}
const readfeet = (robot) => {
    return new Promise((resolve, reject) => {
        fs.readFile('feet.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(robot += `${data}\n`);
            }
        });
    });
}
const writeRobot = (data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile('robot.txt', data, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve("Success");
            }
        });
    });
}

readhead().then((res) => {
    return readbody(res);
}).then((res) => {
    return readleg(res);
}).then((res) => {
    return readfeet(res);
}).then((res) => {
    return writeRobot(res);
}).then(res=>{
    console.log(res)
}).catch(err => {
    console.log(err);
});