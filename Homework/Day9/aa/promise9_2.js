'use strict'

const fs = require('fs');

const readhead = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('head.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
const readbody = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('body.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
const readleg = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('leg.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
const readfeet = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('feet.txt', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
const writeRobot = (data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile('robot.txt', data, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve("Success");
            }
        });
    });
}

let robot ="";
readhead().then((res) => {
    robot +=`${res}\n`;
    return readbody();
}).then((res) => {
    robot +=`${res}\n`;
    return readleg();
}).then((res) => {
    robot +=`${res}\n`;
    return readfeet();
}).then((res) => {
    robot +=`${res}`;
    return writeRobot(robot);
}).then(res=>{
    console.log(res)
}).catch(err => {
    console.log(err);
});
