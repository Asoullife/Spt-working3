const step = async () => {
    try {
        const getRepos = await $.get("https://api.github.com/users/pariyathida/repos");
        const topContributors = await getName(getRepos);
        createTable(topContributors);
    } catch (error) {
        console.error(error);
    }
}
const getName = async (Repos) => {
    let commit = [];
    let topContributorsCount = [];
    for (let i = 0; i < Repos.length; i++) {
        if (Repos[i].name !== 'SEproject') {
            commit[i] = await $.get(`https://api.github.com/repos/pariyathida/${Repos[i].name}/commits`);
            for (let j = 0; j < commit[i].length; j++) {
                let name = commit[i][j].commit.author.name;
                if (!isNaN(topContributorsCount[name])) {
                    topContributorsCount[name]++;
                }
                else {
                    topContributorsCount[name] = 1;
                }
            }
        }
    }
    return (topContributorsCount);
}
const createTable = (data) => {
    let maxComit = 0;
    let name ="";
    let table = ``;
    for (let i in data) {
        table += `<tr><td>${i}</td><td>${data[i]}</td></tr>`;
        if (maxComit < data[i]) {
            maxComit = data[i];
            name = i;
        }
    }
    $("#max").append(`<p>Maxcommit : ${name}</p><p>commit :${maxComit}</p>`)
    $("#table").append(table);
}