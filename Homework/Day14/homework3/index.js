const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const render = require('koa-ejs');
const path = require('path');
const mysql = require('mysql2/promise');

const app = new Koa();
const router = new Router();

render(app,{
    root:path.join(__dirname,'views'),
    layout:'template',
    viewExt:'ejs',
    cache: false,
    debug:true
});

router.get('/',async(ctx,next)=>{
    const pool = await mysql.createPool({
        connectionLimit:10,
        host:'localhost',
        user:'root',
        password:'',
        database:'online_course'
    });
    const [rows,fields] = await pool.query(`SELECT students.name as student_name,courses.name as course_name,courses.detail as detail,
                                            courses.price as price,coalesce(instructors.name,'No Instructor') as instructors
                                            FROM enrolls
                                            LEFT JOIN courses on courses.id = enrolls.course_id
                                            LEFT JOIN students on students.id = enrolls.student_id
                                            LEFT JOIN instructors on instructors.id = courses.teach_by
                                            UNION
                                            SELECT 'Total','','',SUM(courses.price),''
                                            FROM enrolls
                                            LEFT JOIN courses on courses.id = enrolls.course_id`);
    const title = 'homework3';
    await ctx.render('index',{rows,title});
});

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);