const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const render = require('koa-ejs');
const path = require('path');
const mysql = require('mysql2/promise');

const app = new Koa();
const router = new Router();

render(app,{
    root:path.join(__dirname,'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false,
    debug: true
});

router.get('/',async(ctx,next)=>{
    const pool = await mysql.createPool({
        connectionLimit:10,
        host:'localhost',
        user:'root',
        password:'',
        database:'online_course'
    });
    const [rows,fields] = await pool.query(`SELECT courses.name as Course_Name,COUNT(courses.price) as Value,SUM(courses.price) as Price
                                            FROM enrolls
                                            LEFT JOIN courses on courses.id = enrolls.course_id
                                            GROUP BY courses.id
                                            UNION
                                            SELECT 'Total',COUNT(courses.price),SUM(courses.price) FROM enrolls
                                            LEFT JOIN courses on courses.id = enrolls.course_id`);
    const [rows2] = await pool.query(`SELECT students.name as Students_Name,SUM(courses.price) as Price
                                        FROM enrolls
                                        LEFT JOIN students on students.id = enrolls.student_id
                                        LEFT JOIN courses on courses.id = enrolls.course_id
                                        GROUP BY student_id`);
    console.log({rows2})
    const title = 'Total';
    await ctx.render('index',{rows,rows2,title});
});

app.use(serve(path.join(__dirname,'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);